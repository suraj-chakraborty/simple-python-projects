board = [' ' for x in range(10)]

def insertLetter(letter ,pos):
    board[pos] = letter

def freeSpace(pos):
    return board[pos] == ' '

def printBoard(board):
    print('   |   |   ')
    print(' '+ board[1] + ' | ' + board[2] + ' | ' + board[3])
    print('   |   |   ')
    print('____________')
    print('   |   |   ')
    print(' ' + board[4] + ' | ' + board[5] + ' | ' + board[6])
    print('   |   |   ')
    print('____________')
    print('   |   |   ')
    print(' ' + board[7] + ' | ' + board[8] + ' | ' + board[9])
    print('   |   |   ')
    print('____________')

def isBoardFull(board):
    if board.count(' ') > 1:
        return False
    else:
        return True

def isWinner(b,l):
    return (b[1] == l and b[2] == l and b[3] == l) or (b[4] == l and b[5] == l and b[6] == l) or (b[7] == l and b[8] == l and b[9] == l) or (b[1] == l and b[4] ==l and b[7] == l) or (b[2] == l and b[5] ==l and b[8] == l) or (b[3] == l and b[6] ==l and b[9] == l) or (b[1] == l and b[5] ==l and b[9] == l) or (b[3] == l and b[5] ==l and b[7] == l)

def playerMove():
    run= True
    while run:
        move= input("please select a no to enter 'X' between 1 to 9 ")
        try:
            move = int(move)
            if move > 0 and move < 10:
                if freeSpace(move):
                    run = False
                    insertLetter('X', move)
                else:
                    print("this space is occupied")
            else:
                print("please select a no between 1 to 9")

        except:
            print("Please type a no.")


def computerMove():
    possibleMove = [X for X , letter in enumerate(board) if letter == " " and X !=0 ]
    move = 0

    for let in ['O', 'X']:
        for i in possibleMove:
            boardcopy =  board[:]
            boardcopy[i] = let
            if isWinner(boardcopy, let):
                move = i
                return move
        cornersOpen = []
        for i in possibleMove:
            if i in [1, 3, 7, 9]:
                cornersOpen.append(i)
        if len(cornersOpen) > 0:
            move = selectRandom(cornersOpen)
            return move
        if 5 in possibleMove:
            move = 5
            return move
        edgesOpen = []
        for i in possibleMove:
            if i in [2, 4, 6, 8]:
                edgesOpen.append(i)
        if len(edgesOpen) > 0:
            move = selectRandom(edgesOpen)
            return move

def selectRandom(li):
    import random
    ln=len(li)
    r = random.randrange(0, ln)
    return li[r]

def main():
    print("let's play kata-kati 😂")
    printBoard(board)
    while not(isBoardFull(board)):
        if not(isWinner(board, 'O')):
            playerMove()
            printBoard(board)
        else:
            print("you lose")
            break

        if not(isWinner(board, 'X')):
            move = computerMove()
            if move==0:
                print(" ")
            else:
                insertLetter('O', move)
                print('computer placed an O on position',move,':')
                printBoard(board)
        else:
            print("you win")
            break

    if isBoardFull(board):
        print("it's a tie")

while True:
    x = input("you wanna play again? (y/n)")
    if x.lower() == 'y':
        board= [' ' for x in range(10)]
        print('___________________________')
        main()
    else:
        break